//
//  AddOrderVC.swift
//  ContactRecords
//
//  Created by Minh Chu on 04.03.16.
//  Copyright © 2016 Minh Chu. All rights reserved.
//

import UIKit

class AddContactVC: UIViewController {
    // ------------------------------------------------------------------------------------------
    // MARK: - Vars
   
    @IBOutlet weak var phoneTextField: UITextField!
    @IBOutlet weak var nameTextField: UITextField!
    
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView! {
        didSet {
            activityIndicator.hidden = !isUploading
        }
    }
    
    lazy var api: API = API()
    
    var isUploading: Bool = false
    
    // ------------------------------------------------------------------------------------------
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
    }
    
    // ------------------------------------------------------------------------------------------
    // MARK: - User actions
    
    @IBAction func addButtonClicked(sender: AnyObject) {
        if !userInfoFilled() {
            showAlert("Fill up info", message: "First and Last name must be more than 5 characters, phone number cannot be blank")
        
        } else {
            uploadNewContact()
        }
    }
    
    // ------------------------------------------------------------------------------------------
    // MARK: - API calls
    
    func uploadNewContact() {
        guard let name = nameTextField.text, phone = phoneTextField.text else { return }
      
        activityIndicator.hidden = false
        activityIndicator.startAnimating()
       
        api.postNewContact(name, phoneNumber: phone) { [weak self](success, object) -> Void in
            
            self?.activityIndicator.hidden = true
            self?.activityIndicator.stopAnimating()
            
            if success {
                self?.showAlert("Success", message: "New contact added successfully") { [weak self] _ -> Void in
                    mainQueue {
                        self?.navigationController?.popViewControllerAnimated(true)
                    }
                }
            } else {
                self?.showAlert("Failed", message: "Add new contact failed")
            }
        }
    }
    
    // ------------------------------------------------------------------------------------------
    // MARK: - Helpers
    
    
    func userInfoFilled() -> Bool {
        return phoneTextField.isNotEmpty && nameTextField.text?.characters.count > 5
    }
    
    func showAlert(title: String, message: String, handler: ((UIAlertAction) -> Void)? = nil) {
        // user permission not granted
        let confirmAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: handler)
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .Alert)
       
        alertController.addAction(confirmAction)
        self.presentViewController(alertController, animated: true, completion: nil)
    }
}

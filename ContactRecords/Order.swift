//
//  Order.swift
//  ContactRecords
//
//  Created by Minh Chu on 03.03.16.
//  Copyright © 2016 Minh Chu. All rights reserved.
//

import CoreData
import SwiftyJSON

class Order: NSManagedObject {
    @NSManaged var name: String
    @NSManaged var id: String
    @NSManaged var count: Int64
    @NSManaged var contactID: String

}

extension Order: JSONParser {
    func loadData(json: JSON) {
        name = json["name"].stringValue
        count = json["count"].int64Value
        contactID = json["id"]["parent"]["id"].stringValue
    }
}

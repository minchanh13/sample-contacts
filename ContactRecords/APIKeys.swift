//
//  APIKeys.swift
//  ContactRecords
//
//  Created by Minh Chu on 01.03.16.
//  Copyright © 2016 Minh Chu. All rights reserved.
//

import Foundation

struct APIKeys {
    static let BaseAddress = "https://inloop-contacts.appspot.com"
    static let Contact = "/_ah/api/contactendpoint/v1/contact"
    static let Order = "/_ah/api/orderendpoint/v1/order"
}

extension String {
    
    func withId(id: String) -> String {
        
        return self + "/\(id)"
    }
}
import Foundation
import CoreData
import SwiftyJSON

public protocol JSONParser {
    
    var id : String { get set }
    func loadData(json: JSON)
}


public extension JSONParser where Self: NSManagedObject {
    
    public static func new(json: JSON, customId: String?, context: NSManagedObjectContext) -> Self?  {
        guard var
            returnObject = Self.findOrCreateElement(customId, context: context) as? Self
            else { return nil }
        
        
        if let customId = customId {
            
            returnObject.id = customId
        }
        
        
        returnObject.loadData(json)
        
        return returnObject
    }
    
    public static func id(id: String, context: NSManagedObjectContext) -> Self? {
        return Self.fetchObject(NSPredicate(format: "id==%@", argumentArray: [id]), context: context)
    }
}

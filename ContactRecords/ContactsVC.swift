//
//  ViewController.swift
//  ContactRecords
//
//  Created by Minh Chu on 27.02.16.
//  Copyright © 2016 Minh Chu. All rights reserved.
//

import UIKit
import CoreData

class ContactsVC: UIViewController {
    // ------------------------------------------------------------------------------------------
    // MARK: - Vars
    
    var contactsFetchedResultsController: FetchedResultsController<Contact>!

    lazy var api = API()
    lazy var context: NSManagedObjectContext = NSManagedObjectContext.mainContext
    
    @IBOutlet var tableView: UITableView! {
        didSet {
            tableView.dataSource = self
            tableView.delegate = self
            tableView.registerCell(ContactTVCell)
        }
    }
   
    // ------------------------------------------------------------------------------------------
    // MARK: - Fetched results controllers
    
    func setupContactsFRC() {
        contactsFetchedResultsController = FetchedResultsController(
            predicate:nil,
            sortDescriptors: [NSSortDescriptor(key: "name", ascending: true)],
            managedObjectContext: context,
            sectionNameKeyPath: nil,
            cacheName: nil)
        contactsFetchedResultsController?.delegate = self
        do {
            try contactsFetchedResultsController?.performFetch()
        } catch {
            print ("Error while fetching \(error)")
        }
    }
    
    // ------------------------------------------------------------------------------------------
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
    
        setupContactsFRC()
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        api.getAllContacts()
    }
}

// ------------------------------------------------------------------------------------------
// MARK: - Table view

extension ContactsVC: UITableViewDelegate {
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
       
        let orderInfoVC = UIStoryboard(name: "Main", bundle: NSBundle(forClass: OrderInfoVC.self)).instantiateViewControllerWithIdentifier("OrderInfoVC") as! OrderInfoVC
        orderInfoVC.contactID = contactsFetchedResultsController.fetchedObjectsWithType[indexPath.row].id

        self.navigationController?.pushViewController(orderInfoVC, animated: true)
    }
}

extension ContactsVC: UITableViewDataSource {
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return contactsFetchedResultsController.fetchedObjects?.count ?? 0
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeue(cell: ContactTVCell.self, indexPath: indexPath)!
        
        let contactInfo = contactsFetchedResultsController.fetchedObjectsWithType[indexPath.row]
       
        cell.loadCellInfo(contactInfo.name, phone: contactInfo.phone, imageURL: contactInfo.pictureURL)
        
        return cell
    }  
}

extension ContactsVC: NSFetchedResultsControllerDelegate {
    func controllerDidChangeContent(controller: NSFetchedResultsController) {
        tableView.reloadData()
    }
}


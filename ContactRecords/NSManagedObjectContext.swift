//
//  NSManagedObjectContext.swift
//  ContactRecords
//
//  Created by Minh Chu on 01.03.16.
//  Copyright © 2016 Minh Chu. All rights reserved.
//

import UIKit
import CoreData

extension NSManagedObjectContext {
    
    class var mainContext: NSManagedObjectContext! {
        
        return CoreDataStack.managedObjectContext!
    }
}
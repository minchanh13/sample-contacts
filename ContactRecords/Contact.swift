//
//  Contact.swift
//  ContactRecords
//
//  Created by Minh Chu on 03.03.16.
//  Copyright © 2016 Minh Chu. All rights reserved.
//

import Foundation
import CoreData
import SwiftyJSON

class Contact: NSManagedObject {
    @NSManaged var pictureURL: String
    @NSManaged var name: String
    @NSManaged var id: String
    @NSManaged var phone: String
}

extension Contact: JSONParser {
    func loadData(json: JSON) {
        name = json["name"].stringValue == "" ? "Unknown" : json["name"].stringValue
        phone = json["phone"].stringValue == "" ? "Unknown" : json["phone"].stringValue
        pictureURL = json["pictureUrl"].string ?? ""
        
    }
}

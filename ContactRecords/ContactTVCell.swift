//
//  ContactTVCell.swift
//  ContactRecords
//
//  Created by Minh Chu on 03.03.16.
//  Copyright © 2016 Minh Chu. All rights reserved.
//

import UIKit
import SDWebImage

class ContactTVCell: UITableViewCell {
    @IBOutlet var profileImageView: UIImageView!
    @IBOutlet var nameLabel: UILabel!
    @IBOutlet var phoneLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func prepareForReuse() {
        nameLabel.text = nil
        phoneLabel.text = nil
        profileImageView.image = UIImage(named: "ninja")
    }
    
    func loadCellInfo(name: String, phone: String, imageURL: String) {
        nameLabel.text = name
        phoneLabel.text = phone
        
        if imageURL.isNotEmpty {
            loadImage(imageURL)
        }
    }
    
    func loadImage(urlString: String) {
        guard let url = NSURL(string: "https://inloop-contacts.appspot.com/" + urlString) else { return }
        SDWebImageManager.sharedManager().downloadImageWithURL(url, options: [], progress: nil) { image, error, cacheType, finished, url in
            
            if finished && error == nil {
                self.profileImageView.image = image
            }
        }
    }
}

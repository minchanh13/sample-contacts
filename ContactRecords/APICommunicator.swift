//
//  APICommunicator.swift
//  ContactRecords
//
//  Created by Minh Chu on 01.03.16.
//  Copyright © 2016 Minh Chu. All rights reserved.
//

import Foundation
import SwiftyJSON
import AFNetworking
import CoreData

typealias ApiCompletionBlock = (responseObject: JSON?, error: NSError?, success: Bool) -> Void
typealias ApiSuccessBlock = (success: Bool, responseObject: JSON?) -> Void

enum HTTPMethod {
    case GET
    case POST
}

class API {
    func getAllContacts(successBlock: ApiSuccessBlock? = nil) {
        let request = APIRequest { (responseObject, error, success) -> Void in
            
            if let error = error {
                print("Get all contacts \(error)")
            
            } else if let responseObject = responseObject {
              
                if let contacts = responseObject["items"].array {
                    
                    contacts.forEach {
                        Contact.new($0, customId: $0["id"].stringValue, context: NSManagedObjectContext.mainContext)
                    }
                }
            }
            
            successBlock?(success: success, responseObject: responseObject)

        }
        
        request.method = .GET
        request.path = APIKeys.Contact
        request.execute()
    }
    
    func getOrderInfo(contactID: String, successBlock: ApiSuccessBlock? = nil) {
        let request = APIRequest { (responseObject, error, success) -> Void in
           
            if let error = error {
                print("Get order error \(error)")
        
            } else if let responseObject = responseObject {
            
                if let items = responseObject["items"].array {
                    items.forEach {
                        Order.new($0, customId: $0["id"]["id"].stringValue, context: NSManagedObjectContext.mainContext)
                    }
                }
            }
            
            successBlock?(success: success, responseObject: responseObject)
        }
        
        request.method = .GET
        request.path = APIKeys.Order.withId(contactID)
        request.execute()
    }
    
    func postNewContact(userName: String, phoneNumber: String, successBlock: ApiSuccessBlock? = nil) {
        let request = APIRequest { (responseObject, error, success) -> Void in
            
            if let error = error {
                
                print("Add contact error \(error)")
            }
                
            else if let responseObject = responseObject {
                Contact.new(responseObject, customId: responseObject["id"].stringValue, context: NSManagedObjectContext.mainContext)
            }
            
            successBlock?(success: success, responseObject: responseObject)
        }
        
        request.method = .POST
        request.path = APIKeys.Contact
        request.parameters = [
        
            "name": userName,
            "phone": phoneNumber
        ]
        
        request.execute()
    }
}


class APIRequest {
    
    var path : String? = nil
    var method : HTTPMethod = .GET
    var parameters = [String: AnyObject]()
    var operation: NSURLSessionDataTask?
    let apiRequestHandler : ApiCompletionBlock
    
    init(handler: ApiCompletionBlock) {
        
        apiRequestHandler = handler
    }
    
    func cancel() {
        
        operation?.cancel()
    }
    
    func execute() {
        let manager = AFHTTPSessionManager(baseURL: NSURL(string: APIKeys.BaseAddress)!)
        manager.requestSerializer = AFJSONRequestSerializer()
        manager.responseSerializer = AFJSONResponseSerializer()
        
        guard let path = path else { return }
       
        switch method {
            
        case .GET:

            operation = manager.GET(path, parameters: parameters, progress: nil, success: { (operation, receivedObject) -> Void in
                
                if let receivedObject = receivedObject {
                    self.apiRequestHandler(responseObject: JSON(receivedObject), error: nil, success: true)
                }
            
                }, failure: { (operation, error) -> Void in
            
                    self.apiRequestHandler(responseObject: nil, error: error, success: false)
                    print("API ERROR \(error)")
            })
            
            
        case .POST:
            operation = manager.POST(path, parameters: parameters, progress: nil, success: { (operation, receivedObject) -> Void in
                
                if let receivedObject = receivedObject {
                    self.apiRequestHandler(responseObject: JSON(receivedObject), error: nil, success: true)
                }
                
                }, failure: { (operation, error) -> Void in
                    
                    self.apiRequestHandler(responseObject: nil, error: error, success: false)
                    print("API ERROR \(error)")
                    print(JSON(data: (error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey] as? NSData)!))

            })            
        }
    }
}

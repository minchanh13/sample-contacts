//
//  OrderInfoTVCell.swift
//  ContactRecords
//
//  Created by Minh Chu on 03.03.16.
//  Copyright © 2016 Minh Chu. All rights reserved.
//

import UIKit

class OrderInfoTVCell: UITableViewCell {
    @IBOutlet weak var itemNameLabel: UILabel!
    @IBOutlet weak var itemCountLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func prepareForReuse() {
        itemNameLabel.text = nil
        itemCountLabel.text = nil
    }
    
    func loadCellInfo(name: String, count: Int64) {
        
        itemNameLabel.text = name
        itemCountLabel.text = String(count)
    }
}
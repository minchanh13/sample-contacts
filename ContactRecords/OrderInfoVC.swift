//
//  OrderInfoVC.swift
//  ContactRecords
//
//  Created by Minh Chu on 03.03.16.
//  Copyright © 2016 Minh Chu. All rights reserved.
//

import UIKit
import CoreData

class OrderInfoVC: UIViewController {
    // ------------------------------------------------------------------------------------------
    // MARK: - Vars
    
    var contactID: String!
    var orderFetchedResultsController: FetchedResultsController<Order>!
    var context: NSManagedObjectContext = NSManagedObjectContext.mainContext
    
    lazy var api: API = API()

    @IBOutlet var tableView: UITableView! {
        didSet {
            tableView.dataSource = self
            tableView.registerCell(OrderInfoTVCell)
        }
    }
    
    // ------------------------------------------------------------------------------------------
    // MARK: - Fetched results controller
    
    func setupContactsFRC() {
        orderFetchedResultsController = FetchedResultsController(
            predicate: NSPredicate(format: "contactID==%@", argumentArray: [self.contactID]),
            sortDescriptors: [NSSortDescriptor(key: "id", ascending: true)],
            managedObjectContext: context,
            sectionNameKeyPath: nil,
            cacheName: nil)
        orderFetchedResultsController?.delegate = self
        do {
            try orderFetchedResultsController?.performFetch()
        } catch {
            print ("Error while fetching \(error)")
        }
    }
    
    // ------------------------------------------------------------------------------------------
    // MARK: - Life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setupContactsFRC()
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        getOrderInfo()
    }
    
    // ------------------------------------------------------------------------------------------
    // MARK: - API calls
    
    func getOrderInfo() {
        if let contactID = self.contactID {
            api.getOrderInfo(contactID)
        }
    }
    
}

// ------------------------------------------------------------------------------------------
// MARK: - Tableview

extension OrderInfoVC: UITableViewDataSource {
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return orderFetchedResultsController.fetchedObjectsWithType.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeue(cell: OrderInfoTVCell.self, indexPath: indexPath)!
        
        let orderInfo = orderFetchedResultsController.fetchedObjectsWithType[indexPath.row]
       
        cell.loadCellInfo(orderInfo.name, count: orderInfo.count)
        
        return cell
        
    }
}

extension OrderInfoVC: NSFetchedResultsControllerDelegate {
    func controllerDidChangeContent(controller: NSFetchedResultsController) {
        tableView.reloadData()
    }
}


